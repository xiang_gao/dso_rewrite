#ifndef DSO_IMAGE_AND_EXPOSURE_H
#define DSO_IMAGE_AND_EXPOSURE_H

#include <string>
#include "dso/util/NumType.h"

// image and its exposure parameters
// 带曝光参数的图像

namespace dso
{

class ImageAndExposure
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    inline ImageAndExposure ( int w_, int h_, double timestamp_=0 ) : w ( w_ ), h ( h_ ), timestamp ( timestamp_ ) {
        image = new float[w*h];
        exposure_time=1;
    }
    inline ~ImageAndExposure() {
        delete[] image;
    }

    // copy the exposure time to other 
    inline void copyMetaTo ( ImageAndExposure &other ) {
        other.exposure_time = exposure_time;
    }

    // get a deep copy with image data 
    inline ImageAndExposure* getDeepCopy() {
        ImageAndExposure* img = new ImageAndExposure ( w,h,timestamp );
        img->exposure_time = exposure_time;
        memcpy ( img->image, image, w*h*sizeof ( float ) );
        return img;
    }

    float* image;       // irradiance, between 0 to 256
    int w,h;            // width and height
    double timestamp;   // timestamp
    float exposure_time;// exposure time in ms

};

}

#endif
