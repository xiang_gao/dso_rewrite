#ifndef DSO_FRAME_SHELL_H
#define DSO_FRAME_SHELL_H

#include "dso/util/NumType.h"

namespace dso
{

/** @brief the frame shell used for other caller algorithms
 * 作为外壳，与其他程序交互用的Frame（？）
 */
class FrameShell
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    int id=0;     // INTERNAL ID starting at zero
    int incoming_id=0;   // ID passed into DSO
    double timestamp=0;   // time

    // set once after tracking
    SE3 camToTrackingRef =SE3();        // pose to reference
    FrameShell* trackingRef =nullptr;   // pointer to refrence

    // constantly adapted.
    SE3 camToWorld =SE3();                         // Write: TRACKING, while frame is still fresh; MAPPING: only when locked [shellPoseMutex].
    AffLight aff_g2l;
    bool poseValid =true;

    // statisitcs
    int statistics_outlierResOnThis =0;
    int statistics_goodResOnThis =0;
    int marginalizedAt =-1;
    double movedByOpt =0;

};

}

#endif
