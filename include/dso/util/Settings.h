#ifndef DSO_SETTINGS_H
#define DSO_SETTINGS_H

#include <string>
#include <cmath>

// detail settings in dso
// the variables are defined in src/utils/Settings.cpp 

namespace dso 
{
    
// the config bits in solver 
const int SOLVER_SVD =1;
const int SOLVER_ORTHOGONALIZE_SYSTEM =2;
const int SOLVER_ORTHOGONALIZE_POINTMARG =4;
const int SOLVER_ORTHOGONALIZE_FULL =8;
const int SOLVER_SVD_CUT7 =16;
const int SOLVER_REMOVE_POSEPRIOR =32;
const int SOLVER_USE_GN =64;
const int SOLVER_FIX_LAMBDA =128;
const int SOLVER_ORTHOGONALIZE_X =256;
const int SOLVER_MOMENTUM =512;
const int SOLVER_STEPMOMENTUM =1024;
const int SOLVER_ORTHOGONALIZE_X_LATER =2048;
    
    
// ============== PARAMETERS TO BE DECIDED ON COMPILE TIME =================
const int PYR_LEVELS=6;  // total image pyramids 

extern int pyrLevelsUsed;
extern float setting_keyframesPerSecond;
extern bool setting_realTimeMaxKF;
extern float setting_maxShiftWeightT;
extern float setting_maxShiftWeightR;
extern float setting_maxShiftWeightRT;
extern float setting_maxAffineWeight;
extern float setting_kfGlobalWeight;
extern float setting_idepthFixPrior;
extern float setting_idepthFixPriorMargFac;
extern float setting_initialRotPrior;
extern float setting_initialTransPrior;
extern float setting_initialAffBPrior;
extern float setting_initialAffAPrior;
extern float setting_initialCalibHessian;
extern int setting_solverMode;
extern double setting_solverModeDelta;
extern float setting_minIdepthH_act;
extern float setting_minIdepthH_marg;
extern int setting_margPointVisWindow;
extern float setting_maxIdepth;
extern float setting_maxPixSearch;
extern float setting_desiredImmatureDensity;                    // done
extern float setting_desiredPointDensity;                       // done
extern float setting_minPointsRemaining;
extern float setting_maxLogAffFacInWindow;
extern int setting_minFrames;
extern int setting_maxFrames;
extern int setting_minFrameAge;
extern int setting_maxOptIterations;
extern int setting_minOptIterations;
extern float setting_thOptIterations;
extern float setting_outlierTH;
extern float setting_outlierTHSumComponent;
extern float setting_outlierSmoothnessTH; // higher -> more strict
extern int setting_killOverexposed;
extern int setting_killOverexposedMode;
extern int setting_pattern;
extern float setting_margWeightFac;
extern int setting_discreteSeachItsOnPointActivation;
extern int setting_GNItsOnPointActivation;
extern float setting_SmoothnessErrorPixelTH;
extern float setting_SmoothnessEMinInlierPercentage;
extern float setting_SmoothnessEGoodInlierPercentage;
extern float setting_minTraceQuality;
extern int setting_minTraceTestRadius;
extern float setting_reTrackThreshold;
extern int   setting_minGoodActiveResForMarg;
extern int   setting_minGoodResForMarg;
extern int   setting_minInlierVotesForMarg;
extern float setting_minRelBSForMarg;
extern int setting_photometricCalibration;
extern bool setting_useExposure;
extern float setting_affineOptModeA;
extern float setting_affineOptModeB;
extern float setting_affineOptModeA_huberTH;
extern float setting_affineOptModeB_huberTH;
extern int setting_gammaWeightsPixelSelect;
extern bool setting_relinAlways;
extern bool setting_fixCalib;
extern bool setting_activateAllOnMarg;
extern bool setting_forceAceptStep;
extern float setting_useDepthWeightsCoarse;
extern bool setting_dilateDoubleCoarse;
extern float setting_huberTH;
extern bool setting_logStuff;   // 是否生成日志
extern float benchmarkSetting_fxfyfac;
extern int benchmarkSetting_width;
extern int benchmarkSetting_height;
extern float benchmark_varNoise;
extern float benchmark_varBlurNoise;
extern int benchmark_noiseGridsize;
extern float benchmark_initializerSlackFactor;
extern float setting_frameEnergyTHConstWeight;
extern float setting_frameEnergyTHN;
extern float setting_frameEnergyTHFacMean;
extern float setting_frameEnergyTHFacMedian;
extern float setting_overallEnergyTHWeight;
extern float setting_coarseCutoffTH;
extern float setting_minGradHistCut;
extern float setting_minGradHistAdd;
extern float setting_fixGradTH;
extern float setting_gradDownweightPerLevel;
extern bool  setting_selectDirectionDistribution;
extern int setting_pixelSelectionUseFast;
extern float setting_trace_stepsize;
extern int setting_trace_GNIterations;
extern float setting_trace_GNThreshold;
extern float setting_trace_extraSlackOnTH;
extern float setting_trace_slackInterval;
extern float setting_trace_minImprovementFactor;
extern bool setting_render_displayCoarseTrackingFull;
extern bool setting_render_renderWindowFrames;
extern bool setting_render_plotTrackingFull;
extern bool setting_render_display3D;
extern bool setting_render_displayResidual;
extern bool setting_render_displayVideo;
extern bool setting_render_displayDepth;
extern bool setting_fullResetRequested;
extern bool setting_debugout_runquiet;
extern bool disableAllDisplay;
extern bool disableReconfigure;
extern bool setting_onlyLogKFPoses;
extern bool debugSaveImages;
extern int sparsityFactor;
extern bool goStepByStep;
extern bool plotStereoImages;
extern bool multiThreading;
extern float freeDebugParam1;
extern float freeDebugParam2;
extern float freeDebugParam3;
extern float freeDebugParam4;
extern float freeDebugParam5;           // 用来控制PointFrameResidual::debugPlot()的输出 
extern int benchmarkSpecialOption;

// for debug 
void handleKey(char k);

// patterns 
// 有很多种，见Settings.cpp
extern int staticPattern[10][40][2];
extern int staticPatternNum[10];
extern int staticPatternPadding[10];

const int patternNum=8;
#define patternP staticPattern[8]
const int patternPadding=2;
    
}

#endif