#ifndef DSO_GLOBAL_CALIB_H
#define DSO_GLOBAL_CALIB_H

#include "dso/util/Settings.h"
#include "dso/util/NumType.h"

// the calibration data of each image pyramid, defined as global variables in src/util/GlobalCalib.cpp
// NOTE DSO uses scale=2 pyramid levels, and also assumes the image resultion is multiple of 2
// so please use a resultion that is multiple of 2, otherwise it may probably fail. 
// for example，640x480 image may have at most 6 pyramids: 640x480, 320x240, 160x120, 80x60, 40x30, 20x15

// 全局标定参数
// DSO 使用2作为金字塔缩放倍数，并假设图像长宽除2之后仍是整数，直到不能整除为止
// 例如，640x480 最多可以有六层金字塔:640x480, 320x240, 160x120, 80x60, 40x30, 20x15
// 但是一些不能被2整除的分辨率，就可能只有少数层的金字塔。在少于3层时，dso会发出警告

namespace dso
{

// set other calibration data from the original resolution
void setGlobalCalib ( int w, int h, const Eigen::Matrix3f &K );

extern int wG[PYR_LEVELS], hG[PYR_LEVELS];

extern float fxG[PYR_LEVELS], fyG[PYR_LEVELS],
       cxG[PYR_LEVELS], cyG[PYR_LEVELS];

extern float fxiG[PYR_LEVELS], fyiG[PYR_LEVELS],
       cxiG[PYR_LEVELS], cyiG[PYR_LEVELS];

extern Eigen::Matrix3f KG[PYR_LEVELS],KiG[PYR_LEVELS];

extern float wM3G;
extern float hM3G;

}

#endif
