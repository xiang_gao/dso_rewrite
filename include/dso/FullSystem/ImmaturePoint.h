#ifndef DSO_IMMATURE_POINT_H
#define DSO_IMMATURE_POINT_H

#include "dso/util/NumType.h"
#include "dso/FullSystem/HessianBlocks.h"

namespace dso 
{
    
// 临时用的未成熟地图点残差
struct ImmaturePointTemporaryResidual
{
public:
    ResState state_state;
    double state_energy;
    ResState state_NewState;
    double state_NewEnergy;
    FrameHessian* target;
};

// 未成熟地图点的状态
enum ImmaturePointStatus {
    IPS_GOOD=0,                 // traced well and good
    IPS_OOB,                    // OOB: end tracking & marginalize!
    IPS_OUTLIER,                // energy too high: if happens again: outlier!
    IPS_SKIPPED,                // traced well and good (but not actually traced).
    IPS_BADCONDITION,           // not traced because of bad condition.
    IPS_UNINITIALIZED           // not even traced once.
};

// 未成熟地图点是指，原本位于2D图像平面上的像素点（没有深度），在逐步的跟踪过程中，确立深度这一伟大历史进程
class ImmaturePoint
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    // create a immature point from a frame
    ImmaturePoint(int u_, int v_, FrameHessian* host_, float type, CalibHessian* HCalib);

    // track the immature point 
    ImmaturePointStatus traceOn(
        FrameHessian* frame, const Mat33f &hostToFrame_KRKi, 
        const Vec3f &hostToFrame_Kt, const Vec2f &hostToFrame_affine, 
        CalibHessian* HCalib, bool debugPrint=false );

    // linearize the residual
    double linearizeResidual(
            CalibHessian *  HCalib, const float outlierTHSlack,
            ImmaturePointTemporaryResidual* tmpRes,
            float &Hdd, float &bd,
            float idepth);
    
    // 从一个帧变换到另一个帧？
    float getdPixdd(
            CalibHessian *  HCalib,
            ImmaturePointTemporaryResidual* tmpRes,
            float idepth);

    float calcResidual(
            CalibHessian *  HCalib, const float outlierTHSlack,
            ImmaturePointTemporaryResidual* tmpRes,
            float idepth);
    
    // status and data 
    ImmaturePointStatus lastTraceStatus;
    Vec2f lastTraceUV;
    float lastTracePixelInterval;
    float idepth_GT =0;
    Mat22f gradH;                       // 像素梯度的协方差（hessian）,gradH = [dx, dy] * [dx, dy]^T
    Vec2f gradH_ev;
    Mat22f gradH_eig;
    float energyTH;
    float u,v;                          // the pixel position
    FrameHessian* host;                 // the host frame 
    int idxInImmaturePoints;
    float quality =10000;
    float my_type;
    float idepth_min;
    float idepth_max;

    // statistic values
    float color[MAX_RES_PER_POINT];
    float weights[MAX_RES_PER_POINT];
};



    
}

#endif